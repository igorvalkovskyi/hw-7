//Представлення об'єктів HTML документу
//innerHTML показує HTML об'єкти разом з їхнім вмістом та innerText показує сам зміст HTML об'єктів
//getElementById,getElementByClassName,getElementByTagName,document.quarySelector,document.quarySelectorAll.Найкращий - quarySelectorAll 
const elementByTag = document.getElementsByTagName("p");
console.log(elementByTag);
let ReplaceBackground = function() {
   document.body.style.background = "#ff0000";
}
document.addEventListener("DOMContentLoaded", ReplaceBackground);

const elementById = document.getElementById("optionsList");
console.log(elementById);
const elementByIdParents = document.getElementById("optionsList").parentElement;
console.log(elementByIdParents);
const childNode = document.getElementById("optionsList").childNodes;
console.log(childNode);

childNode.forEach((nodeType,nodeName) => console.log(nodeType,nodeName));

document.getElementById("testParagraph").innerHTML = "This is a paragraph";

 const elementBySelector = document.querySelectorAll(".main-header");
 console.log(elementBySelector);
 elementBySelector.forEach(item => item.classList.add("nav-item"));

 const elementBySelectorAll = document.querySelectorAll(".section-title");
 console.log(elementBySelectorAll);
 elementBySelectorAll.forEach(item => item.classList.remove("section-title"));

